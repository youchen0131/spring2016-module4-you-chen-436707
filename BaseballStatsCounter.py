# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import re
import sys, os
from decimal import Decimal


def search(name):
    for p in players:
        if p.name == name:
            return p

       
def update(name, bats, hits):
    player = search(name)
    if player is None:
        new_player = Player(name)
        new_player.atBats += int(bats)
        new_player.hits += int(hits)
        players.append(new_player)
    else:
        player.atBats += int(bats)
        player.hits += int(hits)
         
        
class Player:
    def __init__(player, name):
        player.name=name
        player.atBats=0
        player.hits=0
        player.score=0
        
    @staticmethod
    def getName(player):
        return player.name
            
    def getHits(player):
        return player.hits
    
    def getScore(player):
        return player.score
        

if len(sys.argv) < 2:
	sys.exit("Usage: %s filename" % sys.argv[0])

filename = sys.argv[1]

if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])


f = open(filename, "r")

players = []
scores = {}

for line in f:
    if re.match(r"^#.*", line):
        continue
    elif re.match(r"^===.*", line):
        continue
    else:
        result = re.match(r"([A-Z][\w\s]+)\sbatted\s([\d])\stimes\swith\s([\d])\shits\sand\s[\d]\sruns", line)
        if result:        
            name = result.group(1)
            bats = result.group(2)
            hits = result.group(3)
            #print ('Match found: ', name, bats, hits)
            update(name, bats, hits)
        
for p in players:
    p.score = "{0:.3f}".format(round(Decimal(float(p.hits)/p.atBats), 3))

ret = sorted(players, key=lambda p: p.score, reverse=True)


for p in ret:
    print (p.name, ":", p.score)    
        
       
f.close()